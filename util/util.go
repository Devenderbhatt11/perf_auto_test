package util

import (
	"bufio"
	"os"
)

//Check ... if error exit
func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func CreateFile(fileName string) {
	fileHandle, err := os.Create(fileName)
	//fmt.Printf("creating new file \n")
	Check(err)

	w := bufio.NewWriter(fileHandle)
	//Write headers
	w.WriteString("Clients,Requests,Successful requests,Network failed,Bad requests failed,Successful requests rate,Read throughput,Write throughput,Test time\n")
	//fmt.Printf("wrote %d bytes\n", n)
	w.Flush()
}

func WriteToFile(fileName string, data string) {
	var fileHandle *os.File
	var err error

	if _, err := os.Stat(fileName); err == nil {
		// path/to/whatever exists
		//fileHandle, err = os.Open(fileName)
		fileHandle, err = os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		//fmt.Printf("file exists open\n")
	} else if os.IsNotExist(err) {
		// path/to/whatever does *not* exist
		fileHandle, err = os.Create(fileName)
		//fmt.Printf("file does not exist create \n")
		Check(err)
	} else {
		fileHandle, err = os.Create(fileName)
		//fmt.Printf("file does not exist create\n")
		Check(err)
	}

	w := bufio.NewWriter(fileHandle)
	w.WriteString(data)
	//fmt.Printf("wrote %d bytes\n", n)
	w.Flush()
	Check(err)
}
