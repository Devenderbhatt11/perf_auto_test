package main

import (
	"path/filepath"
	"performance_automation/benchmark"
	"performance_automation/fastrequest"
	perfmodel "performance_automation/model/perf_config"
	model "performance_automation/model/rest_config"
	"performance_automation/params"
	"performance_automation/parser"
	"performance_automation/util"
	"sync"
)

func main() {
	params.VarSession = make(map[string]string)
	abs, _ := filepath.Abs(params.PerfFileName)
	var configParser parser.Parser = parser.ConfigParser{PerfFileName: abs}
	var configsPerf []perfmodel.PerfConfig = configParser.ParsePerfConfiguration()
	var mainWg sync.WaitGroup
	for _, configPerf := range configsPerf {
		//fmt.Println("outside routine Perf Test for configured file-", configPerf.FileName)
		mainWg.Add(1)
		go func(configParser parser.Parser, configPerf perfmodel.PerfConfig) {
			//fmt.Println("Perf Test for configured file-", configPerf.ClientCountArr)
			var configRest []model.ConfigureRest = configParser.ParseConfiguration(configPerf.FileName)
			fastRest := fastrequest.FastRest{configParser, make(map[string]string), &sync.RWMutex{}}
			mybench := &benchmark.MyBench{}
			mybench = mybench.InitVar(configPerf.ClientCountArr, int64(configPerf.InitialTrafficTime), int64(configPerf.TrafficTimeStepUp), int64(configPerf.TrafficTimeMax), int64(configPerf.RequestsPerClientPerSec), configPerf.MaxConnPerHost, configPerf.MaxReadTimeout, configPerf.MaxWriteTimeout, configPerf.ResultPath)
			util.CreateFile(configPerf.ResultPath)
			mybench.InitiateRequestAndBenchmark(configRest, &fastRest, configPerf.FileName, &mainWg,configPerf.TimeToReleaseConn)
		}(configParser, configPerf)
	}
	mainWg.Wait()
}
