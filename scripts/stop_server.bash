#!/bin/bash

INPUT_NUM=5
cnt=0

# arr=(54.212.73.123 54.212.8.104 52.11.118.102 54.212.242.18 54.186.243.213)
# with jq
arr=($(./jq -r .rest_servers[] sample.json))
while [ $INPUT_NUM != $cnt ]; do
  echo "Killing go-service in ${arr[$cnt]}"
  ssh ec2-user@${arr[$cnt]} "ps -ef | grep go-service | grep -v grep | awk '{print \$2}' | xargs kill"
  stat_cnt=$(ssh ec2-user@${arr[$cnt]} "netstat -an| grep 6001| grep LISTEN | wc -l")
  if [ $stat_cnt != 0 ]; then
    echo "service running in ${arr[$cnt]} on port 6001 $stat_cnt"
  else
    echo "service not running in ${arr[$cnt]}  $stat_cnt"
  fi
  ((cnt++))
done
