machines=(34.221.86.4 34.220.152.91 54.186.53.147 54.244.86.93 54.186.200.93)
dt=`date '+%d_%m_%Y_%H_%M_%S'`
dir_name=perf_report_generated_on_"$dt"
file_pattern=$1
echo $file_pattern
echo "${machines[@]}"
mkdir ~/"$dir_name"

for i in "${machines[@]}"; do
    mkdir ~/"$dir_name"/result_"$i"
    scp -r ec2-user@"$i":/home/ec2-user/test_configuration/results/"$file_pattern"* ~/"$dir_name"/result_"$i"/.
done

for ((i = 1; i <= "${#machines[@]}"; i++)); do
    file_name="$file_pattern""$i"M.csv
    paths="$(find ~/"$dir_name" -name $file_name)"
    echo result from the $1 machine with file $file_name path: $paths
    echo summing $i machines report
    ./summing_report_script.bash $paths #summing reports 1machines .. n machines
done
mkdir ~/"$dir_name"/summed_up_reports
mv ./summed_up_report_* ~/"$dir_name"/summed_up_reports/
mv ~/"$dir_name" .
