#!/bin/bash
if [ ! -f "./jq" ]; then
    wget http://stedolan.github.io/jq/download/linux64/jq
    chmod +x ./jq
fi
payload=$(./jq .payload sample.json)

touch ./payload.txt 
echo  $payload > ./payload.txt 
echo payload inside file ------
cat ./payload.txt
echo $(./jq -r --arg payload "$payload" '.[0].request_params.val=$payload' events_config.json)   > events_config.json
echo $(./jq  '.[0].request_params.val = (.[0].request_params.val | fromjson)' events_config.json ) > events_config.json
: > ./perfconfig.json 
./jq .perfconfig sample.json  > ./perfconfig.json
echo modified perfconfig.json  with following data from json
cat ./perfconfig.json

echo modified events_config file is as follows--
cat ./events_config.json
./run_series.bash
