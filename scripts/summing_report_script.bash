#!/bin/bash
echo $*
if [ $# -gt 0 ];
then
    files="$(paste $*)" 
    array=()
    days="$(date +'%Y%m%d_%H%M')"
    touch summed_up_report_$#M.csv
    k=0
    while IFS= read -r line
    do
    # echo "$line"
    if [ $k -gt 0 ];
    then
        IFS='	' read -r -a array <<< "$line"
        size="${#array[@]}" 
        for (( i=0; i<$size; ++i)); do
            IFS=',' read -r -a "arrr$i" <<< "${array[$i]}"
        done
        lining=''    
        for (( j=0; j<9; ++j )); do
            sum=0
            if [ $j -ne 8 ]; then    
                for (( p=0; p<$size; ++p)); do
                    sum=$(( sum + arrr$p[$j] ))
                done
                lining=$lining","$sum
            elif (( $j!=7 & $j!=6 ))
            then
                for (( p=0; p<$size; ++p)); do
                    sum=$(( sum + arrr$p[$j] ))
                done
                sum=$(( sum/size ))
                lining=$lining","$sum
            fi
        done
        lining=${lining#?};
        echo  $lining >> summed_up_report_$#M.csv
    else
        IFS='	' read -r -a array <<< "$line"
        echo "Clients,Requests,Successful requests,Network failed,Bad requests failed,Successful requests rate,Test time" >> summed_up_report_$#M.csv
    fi

    k=$((k+1))

    done < <(printf '%s\n' "$files")
fi

