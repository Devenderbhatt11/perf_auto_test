#!/bin/bash
source ./stop_server.bash
source ./stop_perftest.bash
payload=$(ls -ltrh payload.txt | awk '{print $5}') #payload size
pattern=$(./jq .file_pattern ./sample.json | tr -d '"')
payload_size=awg_${payload}_${pattern}
echo pattern is $payload_size
no_of_machines=(1 2 3 4 5)
initialTraffic=$(./jq .[0].initialTrafficTime ./perfconfig.json)
step_up_time=$(./jq -r .[0].trafficTimeStepUp ./perfconfig.json)
max_traffic_time=$(./jq -r .[0].trafficTimeMax ./perfconfig.json)
timeToReleaseConn=$(./jq -r .[0].timeToReleaseConn ./perfconfig.json)
client_count=$(./jq '.[0].clientCountArr | length' ./perfconfig.json)
time_for_each_test=0
echo "inital traffic ${initialTraffic}"
echo "step up traffic ${step_up_time}"
echo "max traffic ${max_traffic_time}"
for ((i = 0; i < $client_count; i++)); do # calculate test time
	if [ $initialTraffic -lt $max_traffic_time ]; then
		initialTraffic=$((initialTraffic + step_up_time))
	fi
	time_for_each_test=$((initialTraffic + time_for_each_test))
	time_for_each_test=$((time_for_each_test + timeToReleaseConn)) # add connection release time
	time_for_each_test=$((time_for_each_test + 10))                #adding each client time buffer
done

echo "Each machine required test time -- ${time_for_each_test}"

for rscnt in "${no_of_machines[@]}"; do # running test for 1..n machines
	echo "starting $rscnt machine(s)"
	source ./start_machine.bash $rscnt
	echo "sleeping for 30 seconds, waiting for Lb to connect to servers"
	sleep 30
	echo "replacing result fileName in perfconfig.json"
	sed -i "s/events_config_.*/events_config_payload${payload_size}_${rscnt}M.csv\"/g" perfconfig.json
	cat perfconfig.json
	echo "starting perftest with ${rscnt} machines"
	source ./start_perftest.bash $rscnt
	echo "sleeping for ${time_for_each_test} for perftest to complete"
	sleep $time_for_each_test
	((rscnt++))
done

./get_fil_fr_remote_and_genrte_summed_up_report.bash events_config_payload${payload_size}_ #scp perf reports from perf server and generate summed up reports
