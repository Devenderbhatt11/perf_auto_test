#!/bin/bash

INPUT_NUM=$1
cnt=0

# arr=(34.221.86.4 34.220.152.91 54.186.53.147 54.244.86.93 54.186.200.93)
# with jq
arr=($(./jq -r .perf_servers[] sample.json))
while [ $INPUT_NUM != $cnt ]; do
  echo "Killing performance_automation in ${arr[$cnt]}"
  scp events_config.json ec2-user@${arr[$cnt]}:/tmp/
  scp perfconfig.json ec2-user@${arr[$cnt]}:/tmp/
  ssh ec2-user@${arr[$cnt]} "cp /tmp/events_config.json /home/ec2-user/test_configuration/"
  ssh ec2-user@${arr[$cnt]} "cp /tmp/perfconfig.json /home/ec2-user/go/src/performance_automation/config/"
  ssh ec2-user@${arr[$cnt]} "ps -ef | grep performance_automation | grep -v grep | awk '{print \$2}' | xargs kill"
  #ssh ec2-user@${arr[$cnt]} "cd /home/ec2-user/go/src/performance_automation;nohup ../../bin/performance_automation > /dev/null 2>&1 &"
  ((cnt++))
done

start_cnt=0
while [ $INPUT_NUM != $start_cnt ]; do
  echo "Starting performance_automation in ${arr[$start_cnt]}"
  ssh ec2-user@${arr[$start_cnt]} "cd /home/ec2-user/go/src/performance_automation;nohup ../../bin/performance_automation > /dev/null 2>&1 &" &
  ((start_cnt++))
done
