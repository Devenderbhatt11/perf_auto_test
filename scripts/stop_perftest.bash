
#!/bin/bash

INPUT_NUM=5
cnt=0

# arr=(34.221.86.4 34.220.152.91 54.186.53.147 54.244.86.93 54.186.200.93)
# with jq 
arr=( $( ./jq -r .perf_servers[] sample.json ) )
while [ $INPUT_NUM != $cnt ]
do
  echo "Killing performance_automation in ${arr[$cnt]}"
  ssh ec2-user@${arr[$cnt]} "ps -ef | grep performance_automation | grep -v grep | awk '{print \$2}' | xargs kill"
  ((cnt++))	
done

