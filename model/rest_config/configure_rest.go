package restmodel

type Configure struct {
	Configuration []ConfigureRest
}

type ConfigureRest struct {
	Id              string            `json:"id"`
	Url             string            `json:"url"`
	Method          string            `json:"method"`
	Host            string            `json:"host"`
	RequestParam    RequestParams     `json:"request_params"`
	RequestHeader   JsonParams        `json:"request_headers"`
	Cookie          []NameValueParams `json:"cookie"`
	RestResult      []Result          `json:"result"`
	Authorization   NameValueParams   `json:"authorization"`
	GlobalVars      []NameVarParams   `json:"global_vars"`
	SessionCookie   NameVarParams     `json:"session_cookie"`
	QueryParams     JsonParams        `json:"queryParams"`
	UrlReplace      JsonParams        `json:"url_replace"`
	GlobalRefParams []GlobalRefParams `json:"global_ref"`
}

type NameVarParams struct {
	Type         string `json:"type"`
	ObjectNumber int    `json:"object_number"`
	Name         string `json:"name"`
	Var          string `json:"var"`
	BaseIndex    int    `json:"baseIndex"`
}

type GlobalRefParams struct {
	Type         string `json:"type"`
	ObjectNumber int    `json:"object_number"`
	KeyName      string `json:"key_name"`
	Var          string `json:"var"`
	BaseIndex    int    `json:"baseIndex"`
}

type NameValueParams struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type RequestParams struct {
	Val       JsonParams  `json:"val"`
	Randomize []Randomize `json:"randomize"`
}

type Randomize struct {
	Type    string `json:"type"`
	Name    string `json:"name"`
	Prefix  string `json:"prefix"`
	Postfix string `json:"postfix"`
	Digits  int    `json:"digits"`
}

type JsonParams map[string]interface{}

type RequestHeaders struct {
	Content_Type  string `json:"Content-Type"`
	Host          string `json:"Host"`
	Authorization string `json:"Authorization"`
}

type Result struct {
	Code              int             `json:"code"`
	Next_id           string          `json:"next_id"`
	NextRequestParams []NameVarParams `json:"next_request_params"`
}

type NextRequestParam struct {
	name      string `json:"name"`
	ReqType   string `json:"type"`
	prev_name string `json:"prev_name"`
}
