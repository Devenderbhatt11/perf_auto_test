package benchmodel

import "github.com/valyala/fasthttp"

type BenchConfiguration struct {
	Requests  int64
	Period    int64
	KeepAlive bool
	MyClient  fasthttp.Client
}

type BenchResult struct {
	Requests      int64
	Success       int64
	NetworkFailed int64
	BadFailed     int64
	EventName     string
	Clients       int
}
