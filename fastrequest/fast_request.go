package fastrequest

import (
	"fmt"
	"math"
	"math/rand"
	benchmodel "performance_automation/model/bench_config"
	model "performance_automation/model/rest_config"
	"performance_automation/parser"
	"sync"

	uuid "github.com/satori/go.uuid"

	"reflect"
	"strconv"

	"encoding/json"

	"strings"

	//"net/http/httptest"
	"github.com/buger/jsonparser"
	"github.com/valyala/fasthttp"
)

type FastRest struct {
	ConfigParser parser.Parser
	VarSession   map[string]string
	Mux          *sync.RWMutex
}

func (tr FastRest) InitiateNextRequest(configureRest model.ConfigureRest, prevConfigRest model.ConfigureRest, allConfigureRest []model.ConfigureRest, benchConfig *benchmodel.BenchConfiguration, benchResult *benchmodel.BenchResult, prevResp []byte) {

	req := fasthttp.AcquireRequest()

	req = tr.setMethod(req, configureRest, prevConfigRest, prevResp)
	req = tr.setHeader(req, configureRest)
	req = tr.setCookie(req, configureRest)
	tr.Mux.Lock()
	req = tr.setContent(req, configureRest)
	tr.Mux.Unlock()
	req = tr.setAuthorization(req, configureRest)

	//fmt.Fprintln(os.Stdout, "\nTEST REQUEST - ", req.URI(), " REQ BODY-", string(req.Body()))

	res := fasthttp.AcquireResponse()

	err := benchConfig.MyClient.Do(req, res)
	benchResult.Requests++
	if err != nil {
		benchResult.NetworkFailed++
		fmt.Println("network error-", err.Error())
		//panic("handle error")
		return
	}

	//body := res.Body()
	//fmt.Println("Response body-", string(body))
	tr.handleResponse(req, res, configureRest, allConfigureRest, benchConfig, benchResult)
	// Do something with body.
	fasthttp.ReleaseRequest(req)
	fasthttp.ReleaseResponse(res)

	//benchmark.InitiateRequestAndBenchmark(configureRest, tr.getGlobalVarsValue("global_vars.session"))

	//fmt.Fprintln(os.Stdout, "Ending execution-", repos.Headers())
}

func (tr FastRest) handleResponse(req *fasthttp.Request, resp *fasthttp.Response, configureRest model.ConfigureRest, allConfigureRest []model.ConfigureRest, benchConfig *benchmodel.BenchConfiguration, benchResult *benchmodel.BenchResult) {
	contentType := strings.ToLower(strings.Replace(string(resp.Header.Peek("Content-Type")), " ", "", -1))
	contentType = strings.Split(contentType, ";")[0]
	fmt.Println("header-", contentType, " response code -", resp.StatusCode()) // " response-", string(resp.Body()), " ")
	switch contentType {
	case "application/json;charset=utf-8":
		fallthrough
	case "application/json":
		/*tokenmap := resp.Body()
		var tokenMap []map[string]interface{}
		tr.VarSession = make(map[string]string)

		if string(resp.Body()) != "" {
			err := json.Unmarshal(tokenmap, &tokenMap)
			if err != nil {
				panic(err)
			}
		}*/
		//token := tokenMap["token"]
		/*fmt.Println(" ")
		fmt.Fprintlun n(os.Stdout, "RESPONSE DATA-", string(tokenmap))
		fmt.Printf("\n\n response code-", resp.StatusCode())
		fmt.Println(" ----------------------------------- ")
		fmt.Println(" ")*/

		if resp.StatusCode() == 200 {
			benchResult.Success++
			// if login assign auth token to global var sessionId
			/*if configureRest.Id == "login" {
				var tokenPlaceholder = configureRest.GlobalVars[0].Name
				params.VarSession[configureRest.GlobalVars[0].Var] = tokenMap[tokenPlaceholder].(string)
			}*/

			tr.handleGlobalVars(configureRest, resp, req)

			//fmt.Fprintln(os.Stdout, "fetching next_id for processing-", configureRest.RestResult[0].Next_id)

			nextConfigureRest := tr.fetchNextConfigRest(allConfigureRest, configureRest.RestResult[0].Next_id)
			//fmt.Println("Resp 200 fetching next_id for processing-", nextConfigureRest.Id)
			//fmt.Println("Response-", string(resp.Body()))
			if nextConfigureRest.Id == "" {
				//fmt.Println("Exiting test")
			} else {
				tr.InitiateNextRequest(nextConfigureRest, configureRest, allConfigureRest, benchConfig, benchResult, resp.Body())
			}
		} else {
			fmt.Println("header-", contentType, " response code -", resp.StatusCode())
			benchResult.BadFailed++
		}
		break
	case "text/html;charset=iso-8859-1":
		fallthrough
	case "text/plain":
		fallthrough
	case "text/html":
		switch resp.StatusCode() {
		case 200:
			benchResult.Success++
			tr.handleGlobalVars(configureRest, resp, req)
			nextConfigureRest := tr.fetchNextConfigRest(allConfigureRest, configureRest.RestResult[0].Next_id)
			//fmt.Println("Resp 200 fetching next_id for processing-", nextConfigureRest.Id)
			//fmt.Println("Response-", string(resp.Body()))
			if nextConfigureRest.Id == "" {
				//fmt.Println("Exiting test")
			} else {
				tr.InitiateNextRequest(nextConfigureRest, configureRest, allConfigureRest, benchConfig, benchResult, resp.Body())
			}
			break
		case 301:
			fallthrough
		case 302:
			benchResult.Success++
			//nextConfigureRest := tr.fetchNextConfigRest(allConfigureRest, configureRest.RestResult[0].Next_id)
			//fmt.Println("Resp 302 fetching handling redirection")
			//fmt.Println("Response-", resp.Body())
			tr.handleRedirection(resp, configureRest, allConfigureRest, benchConfig, benchResult)
			break
		default:
			benchResult.BadFailed++
			//fmt.Println("status code-", resp.StatusCode())
		}
		break
	default:
		fmt.Println("Content-Type-", string(resp.Header.Peek("Content-Type")))
	}
}

func (tr FastRest) handleRedirection(resp *fasthttp.Response, configureRest model.ConfigureRest, allConfigureRest []model.ConfigureRest, benchConfig *benchmodel.BenchConfiguration, benchResult *benchmodel.BenchResult) {
	location := string(resp.Header.Peek("Location"))
	//fmt.Println("Location redirect - ", location)
	req := fasthttp.AcquireRequest()

	req.Header.SetMethod("GET")
	req.Header.SetRequestURI(location)
	res := fasthttp.AcquireResponse()

	err := benchConfig.MyClient.Do(req, res)
	benchResult.Requests++
	if err != nil {
		benchResult.NetworkFailed++
		return
		//panic("handle error")
	}

	//body := res.Body()
	//fmt.Println("Response body-", string(body))
	tr.handleResponse(req, res, configureRest, allConfigureRest, benchConfig, benchResult)
	fasthttp.ReleaseRequest(req)
	// Do something with body.
	fasthttp.ReleaseResponse(res)
}

func (tr FastRest) handleGlobalVars(configureRest model.ConfigureRest, resp *fasthttp.Response, req *fasthttp.Request) {
	for _, gv := range configureRest.GlobalVars {
		switch gv.Type {
		case "body":
			{
				respTags := strings.Split(gv.Name, ".")
				//fmt.Println("gv.ObjectNumber-", gv.ObjectNumber)
				if gv.ObjectNumber == 0 {
					valToCache, _, _, _ := jsonparser.Get(resp.Body(), respTags...)
					//fmt.Println("val to cache - ", string(valToCache))
					tr.Mux.Lock()
					tr.VarSession[gv.Var] = string(valToCache)
					tr.Mux.Unlock()
				}
				var respObjCount int = 1
				jsonparser.ArrayEach(resp.Body(), func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
					//fmt.Println("object number -", gv.ObjectNumber, " respObjCount-", respObjCount)
					if gv.ObjectNumber == respObjCount {
						tr.Mux.Lock()
						tr.VarSession[gv.Var], _ = jsonparser.GetString(value, respTags...)
						//fmt.Println("val to cache - ", tr.VarSession[gv.Var], " response body-", string(value), " respTags-", respTags)
						tr.Mux.Unlock()
					}
					respObjCount++
				})
			}
		case "url":
			{
				reqUrl := string(req.URI().FullURI())
				reqArrUrl := strings.Split(reqUrl, "/")
				tr.Mux.Lock()
				tr.VarSession[gv.Var] = reqArrUrl[gv.BaseIndex]
				tr.Mux.Unlock()
			}
		case "request_body":
			{
				respTags := strings.Split(gv.Name, ".")
				jsonval, _ := json.Marshal(configureRest.RequestParam.Val)
				val, _ := jsonparser.GetString(jsonval, respTags...)
				tr.Mux.Lock()
				tr.VarSession[gv.Var] = val
				tr.Mux.Unlock()
				//fmt.Println("vehicle number -", val, " gv.Var-", gv.Var, " vlp-", tr.VarSession["vlp"], " configureRest.RequestParam.Val-", configureRest.RequestParam.Val)

			}
		default:
			fmt.Println("handleGlobalVars")
		}
	}
}

func (tr FastRest) handleGlobalRefs(json []byte, GlobalRefs []model.GlobalRefParams, reftype string) []byte {
	//fmt.Println("reqTags-", reqTags, " tagname-", Random.Name)
	var modifiedJSON string = string(json)
	var formQueryVal string
	for _, GlobalRef := range GlobalRefs {
		if reftype != GlobalRef.Type {
			continue
		}
		switch reftype {
		case "request_params":
			reqTags := strings.Split(GlobalRef.KeyName, ".")
			modified_value := "\"" + tr.VarSession[GlobalRef.Var] + "\""
			//fmt.Println("refval-", modified_value, " keyname-", GlobalRef.KeyName)
			modifiedJS, _ := jsonparser.Set([]byte(modifiedJSON), []byte(string(modified_value)), reqTags...)
			//fmt.Println("modifiedJS-", string(modifiedJS))
			modifiedJSON = string(modifiedJS)
			break
		case "url":
			/*reqTags := strings.Split(GlobalRef.KeyName, ".")
			modified_value := tr.getGlobalVarsValue(GlobalRef.Var)
			//fmt.Println("randval-", randval)
			modifiedJS, _ := jsonparser.Set([]byte(modifiedJSON), []byte(string(modified_value)), reqTags...)
			modifiedJSON = string(modifiedJS)*/
			break
		case "queryParams":
			//modified_value := tr.getGlobalVarsValue(GlobalRef.Var)
			formQueryVal += GlobalRef.KeyName + "=" + tr.VarSession[GlobalRef.Var] + "&"
			modifiedJSON = formQueryVal
			break
		}
		//fmt.Println("modifiedJSON-", modifiedJSON)
	}
	return []byte(modifiedJSON)
}

func (tr FastRest) fetchNextConfigRest(allConfigureRest []model.ConfigureRest, configureRestID string) model.ConfigureRest {
	for _, v := range allConfigureRest {
		if v.Id == configureRestID {
			return v
		}
	}
	return model.ConfigureRest{}
}

func (tr FastRest) setMethod(e *fasthttp.Request, ConfigureRest model.ConfigureRest, prevConfigRest model.ConfigureRest, prevResp []byte) *fasthttp.Request {
	queryParams := tr.formQueryParams(ConfigureRest, prevConfigRest, prevResp)
	//fmt.Println("query params-", queryParams)
	e.Header.SetMethod(ConfigureRest.Method)
	//e.Header.SetRequestURI(ConfigureRest.Host + ConfigureRest.Url + "?" + queryParams)
	e = tr.formRequestUri(e, ConfigureRest, queryParams)
	return e
}

func (tr FastRest) formRequestUri(e *fasthttp.Request, ConfigureRest model.ConfigureRest, queryParams string) *fasthttp.Request {
	//if ConfigureRest.Url == "" {
	urlBase := ConfigureRest.Url
	urlArrBase := strings.Split(urlBase, "/")
	var newUrl string
	for i, urlVal := range urlArrBase {
		baseVal := strings.Split(urlVal, ",")
		val := baseVal[0]
		if strings.Contains(val, "{") && strings.Contains(val, "}") {
			placeholder := val[1:strings.IndexByte(val, '}')]
			lat := tr.getGlobalVarsValue(placeholder)
			//there is an integer offset
			var result string
			if len(baseVal) == 2 {
				intr1, _ := strconv.ParseFloat(lat, 64)
				intr2, _ := strconv.ParseFloat(baseVal[1], 64)
				result = fmt.Sprintf("%f", intr1+intr2)
				//fmt.Println("intr1-", intr1, " intr2-", intr2)
			} else {
				result = lat
			}
			if i == len(urlArrBase)-1 {
				newUrl += result
				break
			} else {
				newUrl += result + "/"
			}
		} else {
			if i == len(urlArrBase)-1 {
				newUrl += val
				break
			} else {
				newUrl += val + "/"
			}
		}
	}
	//fmt.Println("New Url-", ConfigureRest.Host+newUrl+"?"+queryParams)
	e.Header.SetRequestURI(ConfigureRest.Host + newUrl + "?" + queryParams)
	return e
	//} else {
	/*	fmt.Println("New Url-", ConfigureRest.Host+ConfigureRest.Url+"?"+queryParams)
		e.Header.SetRequestURI(ConfigureRest.Host + ConfigureRest.Url + "?" + queryParams)
		return e
	}*/
}

func (tr FastRest) setHeader(e *fasthttp.Request, ConfigureRest model.ConfigureRest) *fasthttp.Request {
	for k, v := range ConfigureRest.RequestHeader {
		//fmt.Printf("key[%s] value[%s]\n", k, v)
		e.Header.Set(k, v.(string))
	}
	return e
}

func (tr FastRest) setCookie(e *fasthttp.Request, ConfigureRest model.ConfigureRest) *fasthttp.Request {
	for _, v := range ConfigureRest.Cookie {
		//fmt.Printf("key[%s] value[%s]\n", v.Name, v.Value)
		e.Header.SetCookie(v.Name, v.Value)
	}
	if ConfigureRest.SessionCookie.Name != "" {
		e.Header.SetCookie(ConfigureRest.SessionCookie.Name, tr.getGlobalVarsValue(ConfigureRest.SessionCookie.Var))
	}
	return e
}

func (tr FastRest) setContent(e *fasthttp.Request, ConfigureRest model.ConfigureRest) *fasthttp.Request {
	switch ConfigureRest.RequestHeader["Content-Type"] {
	case "text/plain":
		fallthrough
	case "application/json":
		{
			if len(ConfigureRest.RequestParam.Val) > 0 {
				jsonString, _ := json.Marshal(ConfigureRest.RequestParam.Val)
				jsonString = tr.handleRandomization(jsonString, ConfigureRest.RequestParam.Randomize)
				jsonString = tr.handleGlobalRefs(jsonString, ConfigureRest.GlobalRefParams, "request_params")
				//tr.Mux.Lock()
				json.Unmarshal(jsonString, &ConfigureRest.RequestParam.Val)
				//tr.Mux.Unlock()
				//fmt.Println("modified content based on randomization-", string(jsonString))
				e.SetBody(jsonString)
				//fmt.Println(string(jsonString))
			}
		}
	case "application/x-www-form-urlencoded":
		{
			var formQueryVal string
			var count int = 1
			for k, v := range ConfigureRest.RequestParam.Val {
				if len(ConfigureRest.RequestParam.Val) != count {
					formQueryVal += k + "=" + v.(string) + "&"
				} else {
					formQueryVal += k + "=" + v.(string)
				}
				count++
			}
			e.SetBodyString(formQueryVal)
		}
	default:
		{
			if len(ConfigureRest.RequestParam.Val) > 0 {
				jsonString, _ := json.Marshal(ConfigureRest.RequestParam.Val)
				jsonString = tr.handleRandomization(jsonString, ConfigureRest.RequestParam.Randomize)
				jsonString = tr.handleGlobalRefs(jsonString, ConfigureRest.GlobalRefParams, "request_params")
				//fmt.Println("modified content based on randomization-", string(jsonString))
				e.SetBody(jsonString)
				//fmt.Println(string(jsonString))
			}
		}
	}
	return e
}

func (tr FastRest) handleRandomization(json []byte, RandomizeArr []model.Randomize) []byte {
	var modifiedJSON string = string(json)
	for _, Random := range RandomizeArr {
		reqTags := strings.Split(Random.Name, ".")
		switch Random.Type {
		case "number":
			//fmt.Println("reqTags-", reqTags, " tagname-", Random.Name)
			randomVal := strconv.FormatInt(rand.Int63n(int64((math.Pow(10, float64(Random.Digits))))), 10)
			var randval string = "\"" + Random.Prefix + randomVal + Random.Postfix + "\""
			//fmt.Println("randval-", randval)
			modifiedJS, _ := jsonparser.Set([]byte(modifiedJSON), []byte(string(randval)), reqTags...)
			modifiedJSON = string(modifiedJS)
		case "uuid":
			uid := fmt.Sprintf("%s", uuid.Must(uuid.NewV4()))
			var randval string = "\"" + Random.Prefix + uid + Random.Postfix + "\""
			//fmt.Println("randval uuid-", randval)
			modifiedJS, _ := jsonparser.Set([]byte(modifiedJSON), []byte(string(randval)), reqTags...)
			modifiedJSON = string(modifiedJS)
		default:
			randomVal := strconv.FormatInt(rand.Int63n(int64((math.Pow(10, float64(Random.Digits))))), 10)
			var randval string = "\"" + Random.Prefix + randomVal + Random.Postfix + "\""
			//fmt.Println("randval-", randval)
			modifiedJS, _ := jsonparser.Set([]byte(modifiedJSON), []byte(string(randval)), reqTags...)
			modifiedJSON = string(modifiedJS)
		}
	}
	return []byte(modifiedJSON)
}

func (tr FastRest) setAuthorization(e *fasthttp.Request, ConfigureRest model.ConfigureRest) *fasthttp.Request {
	name := ConfigureRest.Authorization.Name
	value := ConfigureRest.Authorization.Value
	//if global_vars exist then extract value from global map with key as the name after global_vars.
	if strings.Contains(name, "global_vars") {
		globalVarsVal := tr.getGlobalVarsValue(name)
		//fmt.Println("global Vars Val- ", globalVarsVal)
		e.Header.Set("Authorization", globalVarsVal)
	} else {
		e.Header.Set("Authorization", value)
	}
	return e
}

func (tr FastRest) getGlobalVarsValue(placeholder string) string {
	globalValArr := strings.Split(placeholder, ".")
	if len(globalValArr) < 2 {
		return ""
	}
	//fmt.Println("global Vars Name- ", globalValArr[1])
	return tr.VarSession[globalValArr[1]]
}

func (tr FastRest) formQueryParams(ConfigureRest model.ConfigureRest, PrevConfigureRest model.ConfigureRest, prevResp []byte) string {
	var formQueryVal string
	var formQueryValBytes []byte
	var count int = 1
	formQueryValBytes = tr.handleGlobalRefs([]byte(""), ConfigureRest.GlobalRefParams, "queryParams")
	formQueryVal = string(formQueryValBytes)
	//fmt.Println("Query params from global ref", formQueryVal)
	for k, v := range ConfigureRest.QueryParams {
		//fmt.Printf("value type - %s", reflect.TypeOf(v))
		switch v.(type) {
		case string:
			if len(ConfigureRest.QueryParams) != count {
				formQueryVal += k + "=" + v.(string) + "&"
			} else {
				formQueryVal += k + "=" + v.(string)
			}
			count++
		case float64:
			if len(ConfigureRest.QueryParams) != count {
				formQueryVal += k + "=" + strconv.Itoa(int(v.(float64))) + "&"
			} else {
				formQueryVal += k + "=" + strconv.Itoa(int(v.(float64)))
			}
			count++
		case map[string]interface{}:
			val := v.(map[string]interface{})["previous_request"].(bool)
			if val {
				next_req_param_pos := v.(map[string]interface{})["next_request_params_pos"].(float64)
				//0 because 200 ok response
				key := PrevConfigureRest.RestResult[0].NextRequestParams[int(next_req_param_pos)].Name
				resp_arr_index := PrevConfigureRest.RestResult[0].NextRequestParams[int(next_req_param_pos)].ObjectNumber
				respTags := strings.Split(key, ".")

				var arrCount int = 0
				jsonparser.ArrayEach(prevResp, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
					//fmt.Println("resp_arr_index-", resp_arr_index, " offset-", offset)
					if resp_arr_index == arrCount {
						queryVal, _ := jsonparser.GetString(value, respTags...)
						//fmt.Println("key-",key)
						if len(ConfigureRest.QueryParams) != count {
							formQueryVal += key + "=" + queryVal + "&"
						} else {
							formQueryVal += key + "=" + queryVal
						}
					}
					arrCount++
				})
			}
			count++
			//fmt.Printf("value type - %s", reflect.TypeOf(v))
			break
		default:
			fmt.Printf("value type - %s", reflect.TypeOf(v))
			//fmt.Println("value type -", v.(type))
		}
	}
	//fmt.Println("form query value -", formQueryVal) //, "  prev resp- ", string(prevResp))
	return formQueryVal
}

func (tr FastRest) getValueFromPlaceholder(placeholder string) string {
	baseVal := strings.Split(placeholder, ",")
	val := baseVal[0]
	var result string
	if strings.Contains(val, "{") && strings.Contains(val, "}") {
		placeholder := val[1:strings.IndexByte(val, '}')]
		lat := tr.getGlobalVarsValue(placeholder)
		//there is an integer offset

		if len(baseVal) == 2 {
			intr1, _ := strconv.ParseFloat(lat, 64)
			intr2, _ := strconv.ParseFloat(baseVal[1], 64)
			result = fmt.Sprintf("%f", intr1+intr2)
			//fmt.Println("intr1-", intr1, " intr2-", intr2)
		} else {
			result = lat
		}
	}
	return result
}
