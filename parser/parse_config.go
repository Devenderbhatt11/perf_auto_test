package parser

import (
	"encoding/json"
	"io/ioutil"
	perfmodel "performance_automation/model/perf_config"
	model "performance_automation/model/rest_config"
	"performance_automation/util"
)

//Parser ... has function ParseConfiguration
type Parser interface {
	ParseConfiguration(fileName string) []model.ConfigureRest
	ParsePerfConfiguration() []perfmodel.PerfConfig
}

//ConfigParser ... has variable fileName
type ConfigParser struct {
	PerfFileName string
}

//ParseConfiguration ... parses the config rest file and returns config rest struct
func (c ConfigParser) ParseConfiguration(fileName string) []model.ConfigureRest {
	dat, err := ioutil.ReadFile(fileName)
	util.Check(err)

	var result []model.ConfigureRest
	json.Unmarshal(dat, &result)
	//fmt.Fprintln(os.Stdout, "unmarshal-", result)
	return result
}

func (c ConfigParser) ParsePerfConfiguration() []perfmodel.PerfConfig {
	dat, err := ioutil.ReadFile(c.PerfFileName)
	util.Check(err)

	var result []perfmodel.PerfConfig
	json.Unmarshal(dat, &result)
	return result
}
