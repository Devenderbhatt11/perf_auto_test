package main

import (
	"fmt"
	"os"
	"performance_automation/model"
	"performance_automation/params"
	"performance_automation/parser"
	"performance_automation/testrest"
	"testing"
	//"net/http/httptest"
)

func TestApi(t *testing.T) {
	//intialize 'var' placeholder map for global variables

	var configParser parser.Parser = parser.ConfigParser{params.TestFileName}
	var configRest []model.ConfigureRest = configParser.ParseConfiguration(params.TestFileName)
	testRest := testrest.TestRest{configParser}
	testRest.InitiateNextRequest(t, configRest[0], model.ConfigureRest{}, configRest)
	fmt.Fprintln(os.Stdout, "Ending execution")
}
