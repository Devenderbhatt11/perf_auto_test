package params

var (
	VarSession     map[string]string
	PerfFileName   string = "./config/perfconfig.json"
	TestFileName   string = "/Users/akshath_shetty/Documents/design/park_jockey_pass_api_flow.json"
	Clients        int    = 1
	ClientCountArr []int  = []int{2}
	TrafficTime    int64  = 1
)
