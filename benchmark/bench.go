package benchmark

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"performance_automation/fastrequest"
	benchmodel "performance_automation/model/bench_config"
	restmodel "performance_automation/model/rest_config"
	"performance_automation/util"
	"runtime"
	"sync"
	"sync/atomic"
	"time"

	"github.com/valyala/fasthttp"
)

type MyBench struct {
	requests                int64
	clients                 int
	url                     string
	urlsFilePath            string
	keepAlive               bool
	postDataFilePath        string
	writeTimeout            int
	readTimeout             int
	authHeader              string
	maxConnectionPerHost    int
	initialPeriod           int64
	stepupPeriod            int64
	maxPeriod               int64
	requestsPerClientPerSec int64
	resultFilePath          string
	ClientCountArr          []int
	MyCon                   *MyConn
}

/*var readThroughput int64
var writeThroughput int64*/

type MyConn struct {
	net.Conn
	readThroughput  int64
	writeThroughput int64
}

func (this *MyConn) Read(b []byte) (n int, err error) {
	len, err := this.Conn.Read(b)
	//fmt.Println("Read ", len, " bytes")
	if err == nil {
		atomic.AddInt64(&this.readThroughput, int64(len))
	}

	return len, err
}

func (this *MyConn) Write(b []byte) (n int, err error) {
	len, err := this.Conn.Write(b)
	//fmt.Println("Wrote ", len, " bytes")
	if err == nil {
		atomic.AddInt64(&this.writeThroughput, int64(len))
	}

	return len, err
}

func (bench *MyBench) InitVar(clientCountArr []int, initPeriodC int64, stepupPeriodC int64, maxPeriodC int64, requestsPerClientPerS int64, maxPerHost int, maxReadTimeout int, maxWriteTimeout int, resultFilePathC string) *MyBench {

	f := flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	f.Int64Var(&bench.requestsPerClientPerSec, "r", requestsPerClientPerS, "Number of requests per client per sec")
	f.IntVar(&bench.clients, "c", 2, "Number of concurrent clients")
	f.StringVar(&bench.url, "u", "", "URL")
	f.StringVar(&bench.urlsFilePath, "f", "", "URL's file path (line seperated)")
	f.BoolVar(&bench.keepAlive, "k", true, "Do HTTP keep-alive")
	f.StringVar(&bench.postDataFilePath, "d", "", "HTTP POST data file path")
	f.IntVar(&bench.writeTimeout, "tw", maxWriteTimeout, "Write timeout (in milliseconds)")
	f.IntVar(&bench.readTimeout, "tr", maxReadTimeout, "Read timeout (in milliseconds)")
	f.StringVar(&bench.authHeader, "auth", "", "Authorization header")
	f.IntVar(&bench.maxConnectionPerHost, "mc", maxPerHost, "Max conn per host")
	f.Int64Var(&bench.initialPeriod, "ic", initPeriodC, "Initial Period of time (in seconds)")
	f.Int64Var(&bench.stepupPeriod, "sc", stepupPeriodC, "Step up period of time (in seconds)")
	f.Int64Var(&bench.maxPeriod, "mx", maxPeriodC, "Max period of time (in seconds)")
	f.StringVar(&bench.resultFilePath, "rfp", resultFilePathC, "Output result in a file")
	bench.ClientCountArr = clientCountArr
	return bench
}

func (bench *MyBench) printResults(results map[int]*benchmodel.BenchResult, startTime time.Time) {
	var requests int64
	var success int64
	var networkFailed int64
	var badFailed int64
	var eventName string
	var clients int

	for _, result := range results {
		requests += result.Requests
		success += result.Success
		networkFailed += result.NetworkFailed
		badFailed += result.BadFailed
		eventName = result.EventName
		clients = result.Clients
	}

	elapsed := int64(time.Since(startTime).Seconds())

	if elapsed == 0 {
		elapsed = 1
	}

	fmt.Println(" ")
	fmt.Printf("Event Name:	%s\n", eventName)
	fmt.Println("----------------------------------------------------------------")
	fmt.Printf("Clients:                        %10d \n", clients)
	fmt.Printf("Requests:                       %10d hits\n", requests)
	fmt.Printf("Successful requests:            %10d hits\n", success)
	fmt.Printf("Network failed:                 %10d hits\n", networkFailed)
	fmt.Printf("Bad requests failed (!2xx):     %10d hits\n", badFailed)
	fmt.Printf("Successful requests rate:       %10d hits/sec\n", success/elapsed)
	fmt.Printf("Read throughput:                %10d bytes/sec\n", bench.MyCon.readThroughput/elapsed)
	fmt.Printf("Write throughput:               %10d bytes/sec\n", bench.MyCon.writeThroughput/elapsed)
	fmt.Printf("Test time:                      %10d sec\n", elapsed)
	val := fmt.Sprintf("%1d,%1d,%1d,%1d,%1d,%1d,%1d,%1d,%1d\n", clients, requests, success, networkFailed, badFailed, success/elapsed, bench.MyCon.readThroughput/elapsed, bench.MyCon.writeThroughput/elapsed, elapsed)
	util.WriteToFile(bench.resultFilePath, val)
	fmt.Println("----------------------------------------------------------------")
}

func readLines(path string) (lines []string, err error) {

	var file *os.File
	var part []byte
	var prefix bool

	if file, err = os.Open(path); err != nil {
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	buffer := bytes.NewBuffer(make([]byte, 0))
	for {
		if part, prefix, err = reader.ReadLine(); err != nil {
			break
		}
		buffer.Write(part)
		if !prefix {
			lines = append(lines, buffer.String())
			buffer.Reset()
		}
	}
	if err == io.EOF {
		err = nil
	}
	return
}

func (bench *MyBench) NewConfiguration(done *sync.WaitGroup) *benchmodel.BenchConfiguration {

	if bench.requestsPerClientPerSec <= 0 && bench.initialPeriod == -1 {
		fmt.Println("Requests or period must be provided")
		flag.Usage()
		os.Exit(1)
	}

	configuration := &benchmodel.BenchConfiguration{
		KeepAlive: bench.keepAlive,
		Requests:  int64((1 << 63) - 1),
	}

	configuration.Period = bench.initialPeriod

	configuration.Requests = bench.requestsPerClientPerSec
	configuration.MyClient.ReadTimeout = time.Duration(bench.readTimeout) * time.Millisecond
	configuration.MyClient.WriteTimeout = time.Duration(bench.writeTimeout) * time.Millisecond
	configuration.MyClient.MaxConnsPerHost = bench.maxConnectionPerHost

	configuration.MyClient.Dial = bench.MyDialer()

	return configuration
}

func setHeader(request *fasthttp.Request, headers restmodel.JsonParams) *fasthttp.Request {
	for k, v := range headers {
		//fmt.Printf(" setHeader key[%s] value[%s]\n", k, v)
		request.Header.Set(k, v.(string))
	}
	return request
}

func (bench *MyBench) MyDialer() func(address string) (conn net.Conn, err error) {
	return func(address string) (net.Conn, error) {
		conn, err := net.Dial("tcp", address)
		if err != nil {
			return nil, err
		}

		bench.MyCon = &MyConn{Conn: conn}
		myConn := bench.MyCon

		return myConn, nil
	}
}

func (bench *MyBench) client(configuration *benchmodel.BenchConfiguration, result *benchmodel.BenchResult, done *sync.WaitGroup, timeoutPeriod int64, allConfigureRest []restmodel.ConfigureRest, fastRest *fastrequest.FastRest) {
	timeTickASec := time.Tick(1 * time.Second)
	timeout := time.After(time.Duration(timeoutPeriod) * time.Second)
	var requestsPerSecCounter int64 = 0

	for {
		select {
		case <-timeout:
			done.Done()
			return
		case <-timeTickASec:
			requestsPerSecCounter = 0
		default:
			if requestsPerSecCounter < bench.requestsPerClientPerSec {
				time.Sleep(500 * time.Millisecond)
				fastRest.InitiateNextRequest(allConfigureRest[0], restmodel.ConfigureRest{}, allConfigureRest, configuration, result, []byte(""))
				requestsPerSecCounter++
			}
		}
	}
}

func (bench *MyBench) InitiateRequestAndBenchmark(allConfigureRest []restmodel.ConfigureRest, fastRest *fastrequest.FastRest, eventName string, mainWg *sync.WaitGroup, timeToReleaseConn int64) {

	flag.Parse()

	goMaxProcs := os.Getenv("GOMAXPROCS")

	if goMaxProcs == "" {
		runtime.GOMAXPROCS(runtime.NumCPU())
	}

	//clientCountArr := []int{100, 200, 300, 400, 500, 1000}
	//fmt.Println("Perf Test for configured file-", bench.ClientCountArr)
	for index, aclients := range bench.ClientCountArr {
		var done sync.WaitGroup
		configuration := bench.NewConfiguration(&done)
		//fmt.Printf("Dispatching %d clients\n", aclients, " initPeriod-", bench.initialPeriod)
		results := make(map[int]*benchmodel.BenchResult)
		startTime := time.Now()
		for i := 0; i < aclients; i++ {
			result := &benchmodel.BenchResult{}
			result.Clients = aclients
			result.EventName = eventName
			results[i] = result
			done.Add(1)
			clonedAllConfigureRest := make([]restmodel.ConfigureRest, len(allConfigureRest))
			copy(clonedAllConfigureRest, allConfigureRest)
			//fmt.Println("clonedAllConfigureRest-", len(clonedAllConfigureRest), " allConfigureRest-", len(allConfigureRest))
			go bench.client(configuration, result, &done, bench.initialPeriod, clonedAllConfigureRest, fastRest)
		}
		if bench.initialPeriod < bench.maxPeriod {
			bench.initialPeriod += bench.stepupPeriod
		}

		//fmt.Println("Waiting for results...")
		done.Wait()
		bench.printResults(results, startTime)

		if index != len(bench.ClientCountArr)-1 {
			fmt.Println("Sleeping for ", timeToReleaseConn, " seconds to start next set of clients")
			time.Sleep(time.Duration(timeToReleaseConn) * time.Second)
		}
	}
	mainWg.Done()
}
